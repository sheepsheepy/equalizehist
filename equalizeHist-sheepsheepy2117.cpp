#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <string>
#include <iomanip>
#include <cmath>
#include <math.h>
#include <cstdio>

using namespace cv;
using namespace std;

int accumulatearr[256];
int frequencyarr[256];

void frequencyCDFarr(Mat src) //count every 0-255
{ 
	int cal;
	for (int k = 0; k < 256; k++)
	{
		cal = 0;
		for (int i = 0; i < src.rows; i++)
		{
			for (int j = 0; j < src.cols; j++)
			{
				if (src.at<uchar>(i, j) == k)
					cal++;
			}
		}
		frequencyarr[k] = cal;
	}
}

void accumulateCDFarr(Mat src) //add every 0-255
{ 
	int cal = 0;
	for (int i = 0; i < 256; i++)
	{
		accumulatearr[i] = cal + frequencyarr[i];
		cal = accumulatearr[i];
	}
}

int CDFmin() //get the CDF minimum value
{ 
	int min = accumulatearr[0];
	for (int i = 0; i < 256; i++)
	{
		if (accumulatearr[i] < min)
			min = accumulatearr[i];
	}
	return min;
}

Mat drawHistogram() //draw the Histogram
{ 
	Mat histogram(256, 256, CV_8U, Scalar(255)); //open a 256*256的Mat

	int maxnum = frequencyarr[0];
	for (int i = 0; i < 256; i++)
	{
		if (frequencyarr[i] > maxnum)
			maxnum = frequencyarr[i]; //get the max value
	}

	for (int i = 0; i < 256; i++)
	{ //use line to draw Histogram
		line(histogram, Point(i, 255), Point(i, 255 - round(frequencyarr[i] * ((0.9 * 256) / maxnum))), Scalar(0)); // make it clear
	}
	return histogram;
}

Mat changeGray(Mat src)
{
	Mat gray = Mat(src.rows, src.cols, CV_8UC1); //open a same size of src Mat & CV_8UC1 equals gray type
	double cal = 0;

	for (int i = 0; i < src.rows; i++)
	{
		for (int j = 0; j < src.cols; j++)
		{
			//Gray = B * 0.114 + G * 0.587 + R * 0.299
			cal = src.at<Vec3b>(i, j)[0] * 0.114; //B
			cal = cal + src.at<Vec3b>(i, j)[1] * 0.587; //G
			cal = cal + src.at<Vec3b>(i, j)[2] * 0.299; //R

			//avoid the value bigger than 255 or smaller than 0
			if (round(cal) < 0) 	//need round() 
				gray.at<uchar>(i, j) = 0;
			else if (round(cal) > 255)
				gray.at<uchar>(i, j) = 255;
			else
				gray.at<uchar>(i, j) = round(cal);
		}
	}

	return gray;
}

Mat Histogramequalization(Mat src)
{ 
	Mat drt = Mat(src.rows, src.cols, CV_8UC1); //Open a same size Mat
	int data, newdata;
	int cdfmin = CDFmin(); //get the min value
	int rownum = src.rows; //get the src hight & length
	int colnum = src.cols;
	for (int i = 0; i < rownum; i++)
	{
		for (int j = 0; j < colnum; j++)
		{
			data = src.at<uchar>(i, j); //get the original value
			drt.at<uchar>(i, j) = ((accumulatearr[data] - cdfmin) * 255 / ((rownum * colnum) - cdfmin)); //after calculate put back to the new Mat
		}
	}
	return drt;
}

int main(int argc, char *argv[])
{
	Mat imgS, imgA, imgB;
	Mat HistogramA, HistogramB; //imgA&imgB's line graph
	imgS = imread("input.jpg", CV_LOAD_IMAGE_UNCHANGED);

	imgA = changeGray(imgS); //change image to gray

	imwrite("grayinput.jpg", imgA);
	
	frequencyCDFarr(imgA); //function count every 0-255
	accumulateCDFarr(imgA); //function add every 0-255

	HistogramA = drawHistogram(); //­original line graph


	imwrite("HistogramA.jpg", HistogramA);

	imgB = Histogramequalization(imgA); //after equalizeHist image

	imwrite("output.jpg", imgB);
	
	frequencyCDFarr(imgB); //function count every 0-255
	accumulateCDFarr(imgB); //function add every 0-255

	HistogramB = drawHistogram(); //after equalizeHist line graph

	imwrite("HistogramB.jpg", HistogramB);

	waitKey(0);

	return 0;
	
}