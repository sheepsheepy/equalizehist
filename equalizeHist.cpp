#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <string>
#include <iomanip>
#include <cmath>
#include <math.h>
#include <cstdio>

using namespace cv;
using namespace std;


int main(int argc, char *argv[])
{
	Mat imgS, imgD;
	imgS = imread("input.jpg", CV_LOAD_IMAGE_GRAYSCALE);
	equalizeHist(imgS, imgD);
	
	imwrite("imgD.jpg", imgD);
	waitKey(0);

	return 0;
	
}